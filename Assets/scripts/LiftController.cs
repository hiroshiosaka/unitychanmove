﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftController: MonoBehaviour
{

    [SerializeField]
    private float speedX = 0;
    [SerializeField]
    private float speedY = 0;
    [SerializeField]
    private float speedZ = 0;

    private Vector3 startPos;

   
    public Vector3 StartPos
    {
        get
        {
            return startPos;
        }

        set
        {
            startPos = value;
        }
    }

    
    public virtual void funcLiftMove(float sx, float sy, float sz) { }


    void Start()
    {
       
        StartPos = transform.localPosition;
    }

    void Update()
    {
       
        funcLiftMove(speedX, speedY, speedZ);
    }
}