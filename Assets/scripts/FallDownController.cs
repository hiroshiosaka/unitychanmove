﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDownController : MonoBehaviour
{
    public GameObject player;

    Vector3 startPos;

    // Start is called before the first frame update
    void Start()
    {
        startPos = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

    }
           private void OnTriggerEnter(Collider other)
        {
            //プレイヤーが落下時
            if (other.gameObject == player)
            {
                //スタート地点に戻る
                player.transform.position = startPos;
            }
            else
            {
                //プレイヤー以外のオブジェクトが落下した際はオブジェクトを消去
                Destroy(other.gameObject);
            }
        }
    }


