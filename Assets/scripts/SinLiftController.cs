﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinLiftController :LiftController
{
    public override void funcLiftMove(float sx, float sy, float sz)
    { 



    float fx = Mathf.Sin(Time.time * sx);
    float fy = Mathf.Sin(Time.time * sy);
    float fz = Mathf.Sin(Time.time * sz);

    transform.localPosition= StartPos + new Vector3(fx, fy, fz);

  
    }
}
