﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RotateLiftController : LiftController
{

    public override void funcLiftMove(float sx, float sy, float sz)
    {
        transform.Rotate(sx, sy, sz);
    }
}