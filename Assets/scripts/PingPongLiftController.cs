﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingPongLiftController: LiftController
{

    public override void funcLiftMove(float sx, float sy, float sz)
    {
            float fx = Mathf.PingPong(Time.time, sx);
            float fy = Mathf.PingPong(Time.time, sy);

            transform.localPosition = StartPos + new Vector3(fx, fy, 0);
    }
}