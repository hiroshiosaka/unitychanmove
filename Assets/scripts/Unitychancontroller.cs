﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unitychancontroller : MonoBehaviour
{
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        this.animator = GetComponent<Animator>();

    }



    private void OnTriggerEnter(Collider other)
    {
        //オブジェクトがGoalの場合
        if (other.gameObject.name == "Goal")
        {
            //勝利モーションを表示
            animator.SetBool("Win", true);
        }
    }
}   

    
