﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeController : MonoBehaviour
{

    public int maxLife = 100;//最大体力
    int currentLife;//初期体力
    Text lifeText;//体力表示テキスト

    //体力回復コルーチン
    IEnumerator CureLife(int cure)
    {
        //最大値まで自動回復
        while (currentLife < maxLife)
        {
            yield return new WaitForSeconds(1.0f);
            currentLife += cure;
            lifeText.text = currentLife.ToString();
        }
    }

    // Use this for initialization
    void Start()
    {
        //現在の体力を表示
        currentLife = 25;
        lifeText = gameObject.GetComponent<Text>();
        //５ずつ体力回復
        StartCoroutine("CureLife", 5);
    }

}